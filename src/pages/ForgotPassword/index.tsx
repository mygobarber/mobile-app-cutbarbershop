import React, { useRef, useCallback } from 'react';
import {
  Image,
  View,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';

import { useNavigation } from '@react-navigation/native';

import { Form } from '@unform/mobile';
import { FormHandles } from '@unform/core';

import Icon from 'react-native-vector-icons/Feather';

import Input from '../../components/Input';
import Button from '../../components/Button';

import logoImg from '../../assets/logo.png';

import { Container, Title, BackTosignIn, BackTosignInText } from './styles';

const ForgotPassword: React.FC = () => {
  const navegation = useNavigation();

  const formRef = useRef<FormHandles>(null);

  const handleForgotPassaword = useCallback((data) => {
    console.log(data);
  }, []);

  return (
    <>
      <KeyboardAvoidingView
        style={{ flex: 1 }}
        behavior={Platform.OS === 'ios' ? 'padding' : undefined}
        enabled
      >
        <ScrollView
          keyboardShouldPersistTaps="handled"
          contentContainerStyle={{ flexGrow: 1 }}
        >
          <Container>
            <Image source={logoImg} />
            <View>
              <Title>Recupere sua Senha</Title>
            </View>
            <Form ref={formRef} onSubmit={handleForgotPassaword}>
              <Input
                name="email"
                icon="mail"
                placeholder="E-mail"
                autoCorrect={false}
                autoCapitalize="none"
                keyboardType="email-address"
              />
            </Form>
            <Button
              onPress={() => {
                formRef.current?.submitForm();
              }}
            >
              Recuperar
            </Button>
          </Container>
          <BackTosignIn onPress={() => navegation.navigate('SignIn')}>
            <Icon name="arrow-left" size={20} color="#f4ede8" />
            <BackTosignInText>Voltar para o Login</BackTosignInText>
          </BackTosignIn>
        </ScrollView>
      </KeyboardAvoidingView>
    </>
  );
};

export default ForgotPassword;
