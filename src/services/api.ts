import axios from 'axios';

// USB DEVICE
// - RODAR SEGUINTE COMANDO PARA LIBERAR A PORTA EM SEU DISPOSITIVO:
// - adb -s <device_id> reverse tcp:3333 tcp:3333

const api = axios.create({
  baseURL: 'http://localhost:3333',
});

export default api;
